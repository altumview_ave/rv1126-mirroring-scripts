#!/usr/bin/env python

import sys, os, re


_script_dir = os.path.abspath( os.path.dirname(__file__) )
if not _script_dir in sys.path:
    sys.path.insert(0, _script_dir)
    import script_common_utils as utils
    del(sys.path[0])
else:
    import script_common_utils as utils


def match_convert_fetch(the_line):
    # 	<remote fetch="https://bitbucket.org/altumview_rv1126/" name="rk"/>
    p = r'^(.*\s+fetch=\")(\S+)(\"\s+.*)$'
    m = re.match(p, the_line)
    if m is not None:
        g = m.groups()
        if len(g) != 3:
            print("Error: match_convert_fetch len g", len(g))
            return None
        g1 = "https://bitbucket.org/altumview_rv1126/"
        return "%s%s%s" % (g[0], g1, g[2])
    return None


def match_convert_project(the_line):
    #   <project dest-branch="rk33/mid/9.0/develop" name="android/platform/external/libdrm" \
    #       path="external/libdrm" revision="6b4258541a585426b128a365be89c11a1c363e02" upstream="rk33/mid/9.0/develop"/>

    # xml project tag
    p = r'^(.*<project\s+.*)$'
    m0 = re.match(p, the_line)
    if m0 is None:
        return None

    # xml project name, revision, upstream
    p = r'^(.*<project.*\s+name=\")(\S+)(\"\s+.*)$'
    m1 = re.match(p, the_line)
    if m1 is None:
        print("Error: match_convert_project m1")
        return None
    g1 = m1.groups()
    if len(g1) != 3:
        print("Error: match_convert_project len g1", len(g1))
        return None
    the_line = g1[2]

    p = r'^(.*\s+revision=\")(\S+)(\"\s+.*)$'
    m2 = re.match(p, the_line)
    if m2 is None:
        print("Error: match_convert_project m2")
        return None
    g2 = m2.groups()
    if len(g1) != 3:
        print("Error: match_convert_project len g2", len(g1))
        return None
    the_line = g2[2]

    p = r'^(.*upstream=\")(\S+)(\".*)$'
    m3 = re.match(p, the_line)
    if m3 is None:
        print("Error: match_convert_project m3")
        return None
    g3 = m3.groups()
    if len(g3) != 3:
        print("Error: match_convert_project len g3", len(g1))
        return None
    the_line = g3[2]

    # xml tag line closing
    p = r'^(.*/*>.*)$'
    m4 = re.match(p, the_line)
    if m4 is None:
        print("Error: match_convert_project m4")
        return None

    # normalize slug-name:
    g11_slug_raw = g1[1].replace('/', '-')
    # special handling: bitbucket slug-name cannot exceed 62 characters
    g11 = utils.slug_name(g11_slug_raw)

    return "%s%s%s%s%s%s%s" % (g1[0], g11, g2[0], "at", g3[0], "at", the_line)


while True:  # scope
    in_file = sys.argv[1]
    if not os.path.isfile(in_file):
        break
    inl_raw = []
    with open(in_file, "r") as inf:
        inl_raw = inf.readlines()
        inf.close()
    for i, x in enumerate(inl_raw):
        outl = x.rstrip()
        while True:  # scope
            ol = match_convert_fetch(x)
            if type(ol) is str:
                outl = ol
                break
            ol = match_convert_project(x)
            if type(ol) is str:
                outl = ol
                break
            break  # scope
        print(outl)

    break  # scope

