#!/usr/bin/env python
# upstream/push-first-branches.py
#   ../repo/repo forall -c "python $(pwd)/../rv1126-mirroring-scripts/upstream/push-first-branches.py"

import subprocess
import os
import sys
import re

# TODO: qy: may change this _config_repo_team to other proper names for the bitbucket workspace
_config_repo_team = "altumview_rv1126"

# TODO: qy: should change the _config_repo_branch_name to something like
#           "qy_altumview_release_1_20201103"
#           "qy_altumview_release_2_20201201"
_config_repo_branch_name = "up_1_20201203" # change to a new branch name for every update

# TODO: qy: put the username and password to the first two lines of this file:
_config_user_pass_file = "settings-push-user-pass"

settings_user_file = _config_user_pass_file

repo_name = os.getenv("REPO_PROJECT")
repo_index = os.getenv("REPO_I")
repo_count = os.getenv("REPO_COUNT")
repo_slug_raw = repo_name.lower()

run_dir = __file__
if run_dir.startswith('/'):
    run_dir = os.path.abspath(os.path.dirname(run_dir))
else:
    run_dir = os.path.abspath(os.path.dirname(os.getcwd() + "/" + run_dir))

run_user, run_pass = None, None
if os.path.isfile(run_dir + "/" + _config_user_pass_file):
    with open(run_dir + "/" + settings_user_file, "r") as f:
        run_user = f.readline().strip() # first line contains the user name
        run_pass = f.readline().strip() # second line contains the password
        tmp_team = f.readline().strip() # thrid line optional team name
        if len(tmp_team) > 3 and not tmp_team.startswith('#'):
            _config_repo_team = tmp_team
        f.close()
else:
    print("Error: the user pass file does not exist.")

repo_slug = re.sub('/', '-', repo_slug_raw)
# special handling: bitbucket slug-name cannot exceed 62 characters
if repo_slug == "rk-prebuilts-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf":
    repo_slug = "rk-preb-gcc-linaro-6.3.1-2017.05-x86_64_arm-linux-gnueabihf"

print("")
print(" REPO_PROJECT %s %s  %s of %s" % (repo_name, repo_slug, repo_index, repo_count))
print("  cwd: %s" % (os.getcwd()))

if run_user is None or run_pass is None or len(str(run_user)) < 3 or len(str(run_pass)) < 3:
    print("Please create file \"%s\" and put git user and pass in it as first two lines." % (
            run_dir + "/" + settings_user_file ))
    sys.exit(1) # fail

def run_cmd(cmds_in, is_git_push=False):
        cmds_sent = [x for x in cmds_in]
        cmds_show = [x for x in cmds_in]
        if is_git_push:
            cmds_sent.append("https://%s:%s@bitbucket.org/%s/%s.git" % (
                    run_user, run_pass, _config_repo_team, repo_slug))
            cmds_show.append("https://%d:%d@bitbucket.org/%s/%s.git" % (
                    len(run_user), len(run_pass), _config_repo_team, repo_slug))

        proc = subprocess.Popen(cmds_sent, shell=False, bufsize=1,
                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        output = []
        while (True):
                # Read line from stdout, break if EOF reached, append line to output
                line = proc.stdout.readline().decode()
                if (line == ""): break
                output.append(line.strip())
        proc.wait()
        rc = proc.returncode

        print("  run_cmd: %s " % ( str(cmds_show) ))
        print("           rc %d output len %d content " % (rc, len(output)))
        for x in output:
                print("        %s" % str(x) )
        return rc, output

rc_all = 0
while True: # scope

    # it must be on a detached head, or a release branch head:
    #   ~/rvbuildroot/rv1126_rv1109/u-boot$ git branch -v
    #   * (no branch) 602b8060f1 spl: mmc: support load image depend on parameter next_stage
    cmds = ["git", "branch", "-v"]
    rc, output = run_cmd(cmds)
    rc_all |= rc
    proper_head = False
    proper_branched = False # already created the release branch
    for x in output:
        if x.startswith("* (no branch") or x.startswith("* (HEAD detached at"):
            proper_head = True
            break
        if x.startswith("* " + _config_repo_branch_name):
            proper_head = True
            proper_branched = True
            break
    if not rc_all == 0 or not proper_head:
        print("Error: failed proper_head")
        rc_all |= 1
        break

    if not proper_branched:
        cmds = ["git", "checkout", "-b", _config_repo_branch_name]
        rc, _ = run_cmd(cmds)
        rc_all |= rc

    cmds = ["git", "checkout", _config_repo_branch_name]
    rc, _ = run_cmd(cmds)
    rc_all |= rc

    cmds = ["git", "branch", "-av"]
    rc, output = run_cmd(cmds)
    rc_all |= rc

    print("    git branch av command : rc %d output len %d content " % (rc, len(output)))
    for x in output:
        segs = str(x).split(' ')
        print("        %s         :: %s" % (str(x), segs[0]) )

    # should care about only "remotes/beiqi/branchname" or "remotes/rockchip-linux/branchname":
    #
    #   ~/rkbuildroot/linux/app/qsetting$ git branch -av
    #   * altumview_1_0720     5bc5db6 try to reconnect wifi
    #     remotes/beiqi/master 5bc5db6 try to reconnect wifi
    #     remotes/m/master     5bc5db6 try to reconnect wifi
    #
    #   ~/rkbuildroot/linux/external/alsa-config$ git branch -av
    #   * altumview_1_0720              566e426 Merge commit '56ad302'
    #     remotes/m/master              566e426 Merge commit '56ad302'
    #     remotes/rockchip-linux/master 566e426 Merge commit '56ad302'
    #
    # on rv1126 sdk should care about remotes/m/branchname or remotes/rk/branchname
    #   ~/rvbuildroot/rv1126_rv1109/u-boot$ git branch -av
    #   * (no branch)         602b8060f1 spl: mmc: support load image depend on parameter next_stage
    #     remotes/m/linux     602b8060f1 spl: mmc: support load image depend on parameter next_stage
    #     remotes/rk/next-dev 07d90e7755 make.sh: add args to assign uboot and trust image size
    # TODO: qy: as I see only branches on rk and m remotes, modify to match your situation.
    for x in output:
        segs = str(x).split(' ')
        rcmds = []
        if len(segs) > 1:
            brn = segs[0].split('/')
            if len(brn) == 3:
                if brn[0] == "remotes":
                    if brn[1] == "m" or brn[1] == "rk":
                        rcmds = [brn[1], brn[2]]
                    elif brn[1] != "m":
                        print("    Error: unknown remote \"%s\". not \"m\" or \"rk\"." % brn[1])
        if len(rcmds) == 2:

            cmds = [ "git", "checkout", "-b", rcmds[1], "%s/%s" % (rcmds[0], rcmds[1]) ]
            rc, _ = run_cmd(cmds)
            ##rc_all |= rc

            cmds = [ "git", "checkout", rcmds[1] ]
            rc, _ = run_cmd(cmds)
            rc_all |= rc

    cmds = ["git", "checkout", _config_repo_branch_name]
    rc, _ = run_cmd(cmds)
    rc_all |= rc

    if not rc_all == 0:
        print("Error: failed pre-push check")
        break

    if len(repo_slug) > 62:
        print("Error: failed slug length check")
        rc_all != 1
        break

    # TODO: qy: uncomment this section to skip all but the last two repos for testing
    #if int(repo_index) < int(repo_count) - 1:
    #    print("skip")
    #    break

    cmds = ["git", "push", "--all"]
    rc, _ = run_cmd(cmds, is_git_push=True)
    rc_all |= rc

    cmds = ["git", "push", "--tags"]
    rc, _ = run_cmd(cmds, is_git_push=True)
    rc_all |= rc

    break # scope

print(" rc_all %d" % rc_all)
sys.exit(rc_all)

